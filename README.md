
* how much time was invested (within the given limit)<br />
    Exactly 8 hours(not including breaks) <br />

* how was the time distributed (concept, model layer, view(s), game mechanics)<br />
    Around 30-mins on game mechanics<br />
    30-40 mins on concept<br />
    2.5 hours on data layer<br />
    The rest was spent on business and view logic <br />

* decisions made to solve certain aspects of the game <br />
    Handling view based on the states <br />

* decisions made because of restricted time<br />
    Using dagger hilt<br />
    Caching loaded data in the ViewModel<br />
    Simple UI/UX<br />
    Not using Jetpack Compose<br />

* what would be the first thing to improve or add if there had been more time<br />
    Cover with the tests<br />
    Definitely improve UI/UX<br />
    Implement proper Caching<br />
    Proper handling of error cases<br />
    The game mechanics, in terms of generating random words.<br />

