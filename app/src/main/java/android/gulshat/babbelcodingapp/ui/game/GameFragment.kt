package android.gulshat.babbelcodingapp.ui.game

import android.animation.ObjectAnimator
import android.gulshat.babbelcodingapp.Constants.FALLING_ANIMATION_IN_MILLISEC
import android.gulshat.babbelcodingapp.Constants.VERTICAL_TRANSLATION_PROPERTY_NAME
import android.gulshat.babbelcodingapp.R
import android.gulshat.babbelcodingapp.databinding.FragmentGameBinding
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.core.animation.doOnEnd
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import com.google.android.material.snackbar.Snackbar
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class GameFragment : Fragment() {

    private val viewModel: GameViewModel by viewModels()
    private val binding
        get() = requireNotNull(_binding)

    private var _binding: FragmentGameBinding? = null
    private var animator: ObjectAnimator? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewModel.start()
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentGameBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupObservers()
        setListeners()
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
        animator?.removeAllListeners()
        animator = null
    }

    private fun setupObservers() {
        viewModel.apply {
            gameLiveData.observe(viewLifecycleOwner, ::updateGame)
            errorLiveData.observe(viewLifecycleOwner) {
                updateLoading(false)
                Snackbar.make(
                    requireView(),
                    it,
                    Snackbar.LENGTH_INDEFINITE
                ).setAction(R.string.snackbar_retry) {
                    viewModel.start()
                }.show()
            }
        }
    }

    private fun setListeners() {
        with(binding) {
            wrongButton.setOnClickListener { onClick(AnswerValue.WRONG) }
            correctButton.setOnClickListener { onClick(AnswerValue.CORRECT) }
        }
    }

    private fun updateGame(game: Game) {
        updateLoading(isVisible = false)
        when (game.state) {
            GameState.NEW -> {
                updateLoading(isVisible = true)
            }
            GameState.PLAYING -> {
                animateFallingWord(binding.fallingTextView, binding.mainWordTextView.y)
            }
            GameState.GAME_OVER -> {
                val builder = MaterialAlertDialogBuilder(requireContext()).apply {
                    setTitle(R.string.dialog_game_over_title)
                        .setMessage(R.string.dialog_game_over_message)
                        .setPositiveButton(
                            R.string.restart_game_yes
                        ) { _, _ -> viewModel.playNewGame() }
                        .setNegativeButton(
                            R.string.restart_game_exit
                        ) { _, _ -> requireActivity().finish() }
                }.create()
                builder.show()
            }
        }
        game.content?.let(::updateGameContentView)
    }

    private fun updateLoading(isVisible: Boolean) {
        binding.progressBar.isVisible = isVisible
    }

    private fun updateGameContentView(gameContent: GameContent) {
        with(binding) {
            mainWordTextView.text = gameContent.wordPair.mainWord
            fallingTextView.text = gameContent.wordPair.translation
            scoreTextView.text = getString(R.string.game_score, gameContent.score)
            attemptsLeftTextView.text = getString(R.string.attempts_left, gameContent.attemptsLeft)
            wrongButton.isVisible = true
            correctButton.isVisible = true
        }
    }

    private fun animateFallingWord(animationTextView: TextView, animationDestination: Float) {
        animator?.removeAllListeners()
        animator = ObjectAnimator.ofFloat(
            animationTextView,
            VERTICAL_TRANSLATION_PROPERTY_NAME,
            0f,
            animationDestination
        ).apply {
            duration = FALLING_ANIMATION_IN_MILLISEC
            doOnEnd {
                viewModel.submitAnswer(Answer.NotGiven)
            }
            start()
        }
    }

    private val onClick: (answerValue: AnswerValue) -> Unit
        get() = { answerValue -> viewModel.submitAnswer(Answer.Given(answerValue)) }
}
