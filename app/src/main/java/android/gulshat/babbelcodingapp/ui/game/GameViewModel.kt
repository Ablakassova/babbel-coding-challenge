package android.gulshat.babbelcodingapp.ui.game

import android.gulshat.babbelcodingapp.Constants.ATTEMPT_DECREMENT
import android.gulshat.babbelcodingapp.Constants.SCORE_DECREMENT
import android.gulshat.babbelcodingapp.Constants.SCORE_INCREMENT
import android.gulshat.babbelcodingapp.Word
import android.gulshat.babbelcodingapp.domain.LoadWordsUseCase
import android.gulshat.babbelcodingapp.handleErrors
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import javax.inject.Inject
import kotlin.random.Random

@HiltViewModel
class GameViewModel @Inject constructor(
    private val loadWordsUseCase: LoadWordsUseCase,
) : ViewModel() {

    private val words = mutableListOf<Word>()

    private var gameMutableLiveData = MutableLiveData(Game())
    private var errorMutableLiveData = MutableLiveData<String>()
    private lateinit var game: Game

    val gameLiveData: LiveData<Game>
        get() = gameMutableLiveData

    val errorLiveData: LiveData<String>
        get() = errorMutableLiveData

    fun start() {
        loadWords()
    }

    fun playNewGame() {
        updateGameState(
            Game().copy(
                state = GameState.PLAYING,
                content = GameContent(wordPair = newPair)
            )
        )
    }

    fun submitAnswer(answer: Answer) {
        val content = game.content
        val newContent = when (answer) {
            is Answer.NotGiven -> {
                content?.copy(attemptsLeft = content.attemptsLeft - ATTEMPT_DECREMENT)
            }
            is Answer.Given -> {
                if (isAnswerRight(answer.value)) {
                    content?.copy(score = content.score + SCORE_INCREMENT)
                } else {
                    content?.copy(score = content.score - SCORE_DECREMENT)
                }
            }
        }

        if (newContent?.isGameOver == true) {
            return updateGameState(game.copy(state = GameState.GAME_OVER, content = newContent))
        }

        updateGameState(game.copy(content = newContent?.copy(wordPair = newPair)))
    }

    private fun loadWords() {
        if (words.isEmpty())
            viewModelScope.launch {
                withContext(Dispatchers.Default) {
                    loadWordsUseCase(Unit)
                        .handleErrors { throwable ->
                            errorMutableLiveData.postValue(throwable.localizedMessage)
                        }
                        .collect { loadedWords ->
                            words.addAll(loadedWords)
                            playNewGame()
                        }
                }
            }
    }

    private fun updateGameState(newGame: Game) {
        gameMutableLiveData.postValue(newGame)
        game = newGame
    }

    private fun isAnswerRight(answerValue: AnswerValue): Boolean {
        return game.content.let {
            (it?.wordPair?.isTranslationCorrect == true && answerValue == AnswerValue.CORRECT) ||
                    (it?.wordPair?.isTranslationCorrect == false && answerValue == AnswerValue.WRONG)
        }
    }

    private val newPair: WordPair
        get() {
            val indexMain = Random.nextInt(words.lastIndex)
            val indexTranslation =
                if (indexMain % 2 == 0) indexMain else Random.nextInt(words.lastIndex)
            return WordPair(
                words[indexMain].textEng.orEmpty(),
                words[indexTranslation].textSpa.orEmpty(),
                words[indexMain].textSpa == words[indexTranslation].textSpa
            )
        }
}
