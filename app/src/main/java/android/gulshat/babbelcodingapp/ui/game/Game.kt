package android.gulshat.babbelcodingapp.ui.game

import android.gulshat.babbelcodingapp.Constants.DEFAULT_ATTEMPTS
import android.gulshat.babbelcodingapp.Constants.DEFAULT_SCORE

enum class GameState {
    NEW,
    PLAYING,
    GAME_OVER
}

data class Game(
    val state: GameState = GameState.NEW,
    val content: GameContent? = null,
)

data class GameContent(
    val score: Int = DEFAULT_SCORE,
    val attemptsLeft: Int = DEFAULT_ATTEMPTS,
    val wordPair: WordPair,
) {
    val isGameOver: Boolean = attemptsLeft == 0 || score == 0
}

data class WordPair(
    val mainWord: String = "",
    val translation: String = "",
    val isTranslationCorrect: Boolean = false,
)

sealed class Answer {
    data class Given(val value: AnswerValue) : Answer()
    object NotGiven : Answer()
}

enum class AnswerValue {
    CORRECT,
    WRONG,
}
