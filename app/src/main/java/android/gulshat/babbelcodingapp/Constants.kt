package android.gulshat.babbelcodingapp

object Constants {
    const val BASE_URL =
        "https://gist.githubusercontent.com/DroidCoder/7ac6cdb4bf5e032f4c737aaafe659b33/raw/baa9fe0d586082d85db71f346e2b039c580c5804/"

    const val FALLING_ANIMATION_IN_MILLISEC = 8000L
    const val VERTICAL_TRANSLATION_PROPERTY_NAME = "translationY"
    const val DEFAULT_ATTEMPTS = 3
    const val DEFAULT_SCORE = 100
    const val SCORE_INCREMENT = 10
    const val SCORE_DECREMENT = 10
    const val ATTEMPT_DECREMENT = 1
}
