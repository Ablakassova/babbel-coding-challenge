package android.gulshat.babbelcodingapp

import android.app.Application
import dagger.hilt.android.HiltAndroidApp

@HiltAndroidApp
class WordsApp : Application()
