package android.gulshat.babbelcodingapp.domain

abstract class UseCase<in Params, Result> {

    abstract suspend fun execute(params: Params): Result

    suspend operator fun invoke(params: Params): Result = execute(params)
}