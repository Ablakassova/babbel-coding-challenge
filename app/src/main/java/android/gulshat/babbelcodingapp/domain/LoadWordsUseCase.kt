package android.gulshat.babbelcodingapp.domain

import android.gulshat.babbelcodingapp.Word
import android.gulshat.babbelcodingapp.data.repository.WordRepository
import kotlinx.coroutines.flow.Flow
import javax.inject.Inject

class LoadWordsUseCase @Inject constructor(
    private val repository: WordRepository,
) : UseCase<Unit, Flow<List<Word>>>() {

    override suspend fun execute(params: Unit): Flow<List<Word>> {
        return repository.loadWords()
    }
}
