package android.gulshat.babbelcodingapp

import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow

fun <T> Flow<T>.handleErrors(onErrorAction: (e: Throwable) -> Unit): Flow<T> = flow {
    try {
        collect { value -> emit(value) }
    } catch (e: Throwable) {
        onErrorAction(e)
    }
}