package android.gulshat.babbelcodingapp.data.repository

import android.gulshat.babbelcodingapp.Word
import android.gulshat.babbelcodingapp.data.network.api.WordApi
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import javax.inject.Inject

interface WordRepository {
    suspend fun loadWords(): Flow<List<Word>>
}

class WordRepositoryImpl @Inject constructor(
    private val wordApi: WordApi,
) : WordRepository {

    override suspend fun loadWords(): Flow<List<Word>> = flow { emit(wordApi.loadWords()) }
}
