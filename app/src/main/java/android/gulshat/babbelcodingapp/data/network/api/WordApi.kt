package android.gulshat.babbelcodingapp.data.network.api

import android.gulshat.babbelcodingapp.Word
import retrofit2.http.GET

interface WordApi {

    @GET("words.json")
    suspend fun loadWords(): List<Word>
}
