package android.gulshat.babbelcodingapp

data class Word(
    val textEng: String?,
    val textSpa: String?,
)
