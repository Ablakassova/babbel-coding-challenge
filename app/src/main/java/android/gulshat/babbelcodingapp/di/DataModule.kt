package android.gulshat.babbelcodingapp.di

import android.gulshat.babbelcodingapp.data.network.api.WordApi
import android.gulshat.babbelcodingapp.data.repository.WordRepository
import android.gulshat.babbelcodingapp.data.repository.WordRepositoryImpl
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
class DataModule {

    @Provides
    @Singleton
    fun provideWordRepository(wordApi: WordApi): WordRepository = WordRepositoryImpl(wordApi)
}
