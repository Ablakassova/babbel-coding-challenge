package android.gulshat.babbelcodingapp.di

import android.gulshat.babbelcodingapp.data.repository.WordRepository
import android.gulshat.babbelcodingapp.domain.LoadWordsUseCase
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ViewModelComponent

@Module
@InstallIn(ViewModelComponent::class)
class DomainModule {

    @Provides
    fun provideLoadWordsUseCase(repository: WordRepository) = LoadWordsUseCase(repository)
}
